package com.booking.go.movietitle;

import com.google.gson.Gson;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.util.CollectionUtils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@SpringBootApplication
public class MovieTitleApplication {

    public static void main(String[] args) {
        SpringApplication.run(MovieTitleApplication.class, args);

        try {
            List<String> movieTitles = getMovieTitles("spiderman");
            if (!CollectionUtils.isEmpty(movieTitles)) {
                movieTitles.forEach(title -> System.out.println(title));
            }
        } catch (Exception e) {
            // Log Exception
        }
    }

    public static String getMovieJson(String substr) throws Exception {
        String url = "https://jsonmock.hackerrank.com/api/movies/search/?Title=";
        URL obj = new URL(url + substr);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET");
        int responseCode = con.getResponseCode();
        if (responseCode == HttpURLConnection.HTTP_OK) { // success
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            return response.toString();
        }
        return null;
    }

    public static List<String> getMovieTitles(String substr) throws Exception {
        List<String> finalTitleList = new ArrayList<>();
        int totalNumberOfPages = getTotalNumberOfPages(substr);
        if(totalNumberOfPages > 0) {
            // Loop through All Pages
            Gson gson = new Gson();
            for (int pageNumber = 1; pageNumber <= totalNumberOfPages; pageNumber++) {
                String jsonResponse = getMovieJson(substr + "&page=" + pageNumber);
                if (null != jsonResponse) {
                    Movie moviePage = gson.fromJson(jsonResponse, Movie.class);
                    List<String> titleList = moviePage.getData().stream().map(MovieData::getTitle).collect(Collectors.toList());
                    finalTitleList.addAll(titleList);
                }
            }
            Collections.sort(finalTitleList);
        }

        return finalTitleList;
    }

    public static int getTotalNumberOfPages(String title) throws Exception {
        Gson gson = new Gson();
        String json = getMovieJson(title);
        if (null != json) {
            Movie movie = gson.fromJson(json, Movie.class);
            return movie.getTotalPages();
        }
        return 0;
    }

}
